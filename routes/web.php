<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//jobs route
// Route::get('/', [App\Http\Controllers\JobController::class, 'index'])->name('index');
Route::view('jobs/create', 'jobs.create')->name('jobs.create');
Route::post('jobs/store', [App\Http\Controllers\JobController::class, 'store'])->name('jobs.store')->middleware('employer');
Route::get('jobs/myjobs/{user}', [App\Http\Controllers\JobController::class, 'myJobs'])->name('jobs.myjobs')->middleware('employer');
Route::get('jobs/{id}/{slug}', [App\Http\Controllers\JobController::class, 'jobDetails'])->name('jobs.details')->middleware('employer');
Route::get('jobs/edit/{user}/{slug}', [App\Http\Controllers\JobController::class, 'edit'])->name('jobs.edit')->middleware('employer');
Route::post('jobs/update/{user}', [App\Http\Controllers\JobController::class, 'update'])->name('jobs.update')->middleware('employer');
Route::get('jobs/apply/job/{job}', [App\Http\Controllers\JobController::class, 'jobApply'])->name('apply')->middleware('seeker');
Route::get('jobs/all-applicants', [App\Http\Controllers\JobController::class, 'applicants'])->name('jobs.applied')->middleware('employer');
Route::get('jobs/myjobs/destroy/{job}', [App\Http\Controllers\JobController::class, 'destroy'])->name('jobs.destroy')->middleware('employer');
Route::get('jobs/alljobs', [App\Http\Controllers\JobController::class, 'alljobs'])->name('alljobs')->middleware('seeker');

//employer route
Route::view('/employer/register', 'auth.emp-register')->name('employer.reg')->middleware('guest');
Route::post('/employer/register', [App\Http\Controllers\EmployerController::class, 'register'])->name('employer.register');


//applicant route
Route::get('/profile/update', [App\Http\Controllers\ProfileController::class, 'UpdateProfile'])->name('profile.update');
Route::view('/profile/create/{id}', 'user.create');
Route::post('/profile/store', [App\Http\Controllers\ProfileController::class, 'store'])->name('profile.store');
Route::post('profile/avatar', [App\Http\Controllers\ProfileController::class, 'avatar'])->name('profile.avatar');
Route::post('profile/resume', [App\Http\Controllers\ProfileController::class, 'resume'])->name('profile.resume');


//company routes

Route::view('/company/profile/{id}', 'company.create')->name('company.create');
Route::post('/company/profile/store', [App\Http\Controllers\CompanyController::class, 'store'])->name('company.store');
Route::post('/company/logo', [App\Http\Controllers\CompanyController::class, 'logo'])->name('company.logo');
// Route::get('/company/profile', [App\Http\Controllers\CompanyController::class, 'company.index')->name('company.profile');