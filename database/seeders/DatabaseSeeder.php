<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Company;
use App\Models\Job;
use App\Models\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::factory()->count(20)->create();
        Company::factory()->count(20)->create();
        Job::factory()->count(20)->create();

        $categories = [
            'Medical','Software','Goverment','NGO'
        ];
        foreach ($categories as $category){
            Category::create(['name'=>$category]);
        }

    }
}
