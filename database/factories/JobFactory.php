<?php

namespace Database\Factories;

use App\Models\Job;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Company;

class JobFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Job::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
          return [
            'user_id' => User::factory(),
            'company_id' => Company::factory(),
            'title' => $name=$this->faker->text,
            'slug' =>str_slug($name),
            'description' =>$this->faker->paragraph(rand(2,10)),
            'category_id' =>rand(0,1),
            'position' =>$this->faker->jobTitle,
            'address' => $this->faker->address,
            'type' => 'Full Time',
            'roles' => $this->faker->text,
            'deadline' => $this->faker->DateTime,
        ];
    }
}
