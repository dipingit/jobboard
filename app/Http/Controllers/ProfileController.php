<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('seeker');
    }

    public function store(Request $request){

        $this->validate($request,[
            'address' => 'required',
            'experience' => 'required',
            'bio' => 'required',
            'phone_number' => 'required|numeric',
        ]);
        $user_id = auth()->user()->id;
        Profile::where('user_id',$user_id)->update([
            'address'=>request('address'),
            'phone_number'=>request('phone_number'),
            'experience'=>request('experience'),
            'bio'=>request('bio'),
        ]);
        return redirect()->back()->with('message','Profile Updated Successfully');
        }

        public function avatar(Request $request){
            $this->validate($request,[
            'avatar' => 'required|mimes:jpg,png,jpeg|max:2048',
        ]);
        $user_id = auth()->user()->id;
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $text = $file->getClientOriginalExtension();
            $fileName=time().'.'.$text;
            $file->move('uploads/avatar',$fileName);
            Profile::where('user_id',$user_id)->update([
                'avatar'=>$fileName
            ]);
            return redirect()->back()->with('avatar','Avatar Uploaded Successfully');
        }
        }

        public function resume(Request $request){

            $this->validate($request,[
            'resume' => 'required|mimes:doc,pdf,docx|max:2048',
        ]);
        $user_id = auth()->user()->id;
        $resume = $request->file('resume')->store('public/files');
        Profile::where('user_id',$user_id)->update([
            'resume'=> $resume
        ]);
        return redirect()->back()->with('resume','Resume Uploaded Successfully');
        }
                
    }
