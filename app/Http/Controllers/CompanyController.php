<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('employer');
    }
  
    public function store(Request $request){

           $this->validate($request,[
            'address' => 'required',
            'phone' => 'required',
            'website' => 'required',
            'slogan' => 'required',
            'description' => 'required',

        ]);

        $user_id = auth()->user()->id;
        Company::where('user_id',$user_id)->update([
            'address'=>request('address'),
            'phone'=>request('phone'),
            'website'=>request('website'),
            'slogan'=>request('slogan'),
            'description'=>request('description'),
        ]);

        return redirect()->back()->with('message','Company Profile Updated Successfully');
    }

    public function logo(Request $request){

        // dd(request()->all());

        $this->validate($request,[
            'logo' => 'required|mimes:jpg,png,jpeg|max:2048',
        ]);

        

        $user_id = auth()->user()->id;
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $text = $file->getClientOriginalExtension();
            $fileName=time().'.'.$text;
            $file->move('uploads/logo',$fileName);
            Company::where('user_id',$user_id)->update([
                'logo'=>$fileName
            ]);
       
            return redirect()->back()->with('logo','Logo Uploaded Successfully');
        }
    }
    
}

