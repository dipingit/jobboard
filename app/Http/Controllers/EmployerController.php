<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\User;

class EmployerController extends Controller
{

    public function register(){

        $user = User::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
            'user_type' => request('user_type'),
        ]);

        Company::create([
            'user_id' => $user->id,
            'cname' => request('cname'),
            'slug'  => str_slug(request('cname')),
        ]);

        return redirect()->to('login');
    }
}
