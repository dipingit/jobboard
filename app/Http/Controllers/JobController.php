<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\User;
use App\Models\JobApplied;
use App\Models\Company;
use DB;
class JobController extends Controller
{

    public function index(){

        $jobs = DB::table('jobs')
             ->join('companies','jobs.user_id','companies.user_id')
             ->select('jobs.*','companies.cname')
             ->get();
            return view('jobs.alljobs',compact('jobs'));
    }
    
    public function jobDetails($id, $slug){ //view job details
        $job_details = Job::find($id);
        return view('jobs.job_details')->with('job_details',$job_details);
    }

    public function edit($id, $slug){
        
        $jobedit = Job::find($id);
        return view('jobs.editjob')->with('jobedit',$jobedit);
    }

    public function update($id){
        $user_id = auth()->user()->id;
        $company = Company::where('user_id',$user_id)->first();
        $company_id = $company->id;
        Job::where('id', $id)->update([
            'user_id' => auth()->user()->id,
            'company_id' => $company_id,
            'title' => request('title'),
            'roles' => request('roles'),
            'description' => request('description'),
            'category_id' => request('category'),
            'position' => request('position'),
            'address' => request('address'),
            'type' => request('type'),
            'deadline' => request('deadline'),
        ]);
        return redirect()->route('jobs.myjobs',auth()->user()->id)->with('success', 'Job Successfully Updated');   
    }

    public function store(){

        $user_id = auth()->user()->id;
        $company = Company::where('user_id',$user_id)->first();
        $company_id = $company->id;
         Job::create([
            'user_id' => $user_id,
            'company_id' => $company_id,
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'roles' => request('roles'),
            'description' => request('description'),
            'category_id' => request('category'),
            'position' => request('position'),
            'address' => request('address'),
            'type' => request('type'),
            'deadline' => request('deadline'),
        ]);

        return Redirect()->to('/jobs/alljobs');
        
    }

  

    public function destroy($id){
        $delete = Job::find($id);
        $delete->delete();
        return redirect()->route('jobs.myjobs',auth()->user()->id)->With('success','Job Deleted Successfully!');

    }


    public function myJobs($id){
        
         $jobs = DB::table('jobs')->where('user_id',$id)->get();
        return view('jobs.myjobs',compact('jobs'));

    }

    public function jobApply($id){
        
        $user_id = auth()->user()->id;
        
        if(auth()->user()->profile->resume == null){
            return redirect()->to("/profile/create/".$user_id)->with(['status' => 0, 'message' => 'Upload your resume first']);;
        }
        else{
            $job = DB::table('job_applied')->where('job_id',$id)->where('user_id',auth()->user()->id)->first();
            if($job){
                return redirect()->to('/jobs/alljobs')->with(['status' => 0, 'message' => 'You have already applied for this job']);
            }
            else{
            $job_id = JobApplied::create([
                'job_id' => $id,
                'user_id' => auth()->user()->id
            ]);
            return redirect()->to('/jobs/alljobs')->with(['status' => 1, 'message' => 'You have applied successfully!']);
            }
        }

    }

    public function applicants(){

        $applicants = Job::has('users')->where('user_id', auth()->user()->id)->get();

        return view('jobs.applicants', compact('applicants'));
    }

    public function allJobs(){

        $keyword = request('title');
        $category = request('category_id');

        if ($keyword && $category) {
            $jobs = Job::where('title', 'LIKE', '%'. $keyword . '%')
                  -> orWhere('category_id', $category)
                  -> paginate(10);
            return view('jobs.alljobs',compact('jobs'));
        }
        else if($keyword && $category==0){
            $jobs = Job::where('title', 'LIKE', '%'. $keyword . '%')
                  ->paginate(10);
            return view('jobs.alljobs', compact('jobs'));
        }
        else if($category){
            $jobs = Job::where('category_id', $category)
                  ->paginate(10);
            return view('jobs.alljobs', compact('jobs'));
        }
        $jobs = Job::paginate(10);
        return view('jobs.alljobs',compact('jobs'));

    }

}
