<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;

    protected $guarded = [];

    // public job_applied(){
    //     return $this->hasOne('App\Models\')
    // }

    public function users(){
        return $this->belongsToMany('App\Models\User', 'job_applied')->withTimestamps();
    }

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }
}
