@extends('layouts.app')
@section('content')
<div class="container">

  <table class="table table-border table-responsive">
    <tr>
      <th>Job Title</th>
      <th>Job Description</th>
      <th>Location</th>
      <th>Salary</th>
      <th>Country</th>
      <th>Action</th>
    </tr>
    @foreach($job as $show)
    <tr>
      <td>{{$show->job_title}}</td>
      <td>{{$show->job_description}}</td>
      <td>{{$show->location}}</td>
      <td>{{$show->salary}}</td>
      <td>{{$show->country}}</td>
      <td><a class="btn btn-danger btn-sm" href="{{route ('emp.delete_job',$show->id)  }}">Delete Job</a>
      </td>
    </tr>
      </table>
     @endforeach
  
</div>
@endsection