@extends('layouts.master')
@section('content')

<div class="site-section bg-light pt-3 pb-3">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Post a Job</div>
          <div class="card-body">
            <form method="post" action="{{ route('jobs.store') }}">
              @csrf
              <div class="form-group">
                <label>Title</label>
                <input type="text"  name= "title" class="form-control">
              </div>
              <div class="form-group">
                <label>Roles</label>
                <input type="text" name= "roles" class="form-control">
              </div> 
              <div class="form-group">
                <label>Description</label>
                <textarea type="text" name= "description" class="form-control"></textarea>
              </div> 
              <div class="form-group">
                <label>Category</label>
                <select type="text" name="category" class="form-control">
                  @foreach (App\Models\Category::all() as $cat)
                  <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Position</label>
                <input type="text" name="position" class="form-control">
              </div>
              <div class="form-group">
                <label>Address</label>
              <textarea type="text" name="address" class="form-control"></textarea>
              </div>
              <div class="form-group">
                <label>Type</label>
                <select type="text" name="type" class="form-control">
                  <option value="fullTime">Full Time</option>
                  <option value="partTime">Part Time</option>
                </select>
              </div>
              <div class="form-group">
                <label>Deadline</label>
                <input type="date" name="deadline" class="form-control">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection