@extends('layouts.master')
@section('content')

<div class="site-section bg-light">
  <div class="container">
    <div class="row justify-content-center p-5">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Edit Your Job</div>
          <div class="card-body">
            <form method="post" action="{{ route('jobs.update', $jobedit->id) }}">
              @csrf
              <div class="form-group">
                <label>Title</label>
                <input type="text"  name= "title" class="form-control" value="{{ $jobedit->title }}">
              </div>
              <div class="form-group">
                <label>Roles</label>
                <input type="text" name= "roles" class="form-control" value="{{ $jobedit->roles }}">
              </div> 
              <div class="form-group">
                <label>Description</label>
                <textarea type="text" name= "description" class="form-control">{{ $jobedit->description }}</textarea>
              </div> 
              <div class="form-group">
                <label>Category</label>
                <select type="text" name="category" class="form-control" value="{{ $jobedit->category }}">
                  @foreach (App\Models\Category::all() as $cat)
                  <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Position</label>
                <input type="text" name="position" class="form-control" value="{{ $jobedit->position }}">
              </div>
              <div class="form-group">
                <label>Address</label>
              <textarea type="text" name="address" class="form-control">{{ $jobedit->address }}</textarea>
              </div>
              <div class="form-group">
                <label>Type</label>
                <select type="text" name="type" class="form-control" value="{{ $jobedit->type }}">
                  <option value="fullTime">Full Time</option>
                  <option value="partTime">Part Time</option>
                </select>
              </div>
              <div class="form-group">
                <label>Deadline</label>
                <input type="date" name="deadline" class="form-control" value="{{ $jobedit->deadline }}">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection