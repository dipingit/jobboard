@extends('layouts.master')
@section('content')
<div class="container">
  <div class="row justify-content-center p-5">
    <div class="col-md-8">
      <div class="card">
          @if (\Session::has('success'))
            <div class="alert alert-success">
               <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true">&times;
                </button>
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
          @endif
      <div class="card-header">All Jobs</div>
  <table class="table table-border table-responsive">
    <tr>
      <th>Job Title</th>
      <th>Roles</th>
      <th>Position</th>
      <th>Type</th>
      <th>Deadline</th>
      <th>Action</th>
    </tr>
    @foreach($jobs as $job)
    <tr>
      <td>{{$job->title}}</td>
      <td>{{$job->roles}}</td>
      <td>{{$job->position}}</td>
      <td>{{$job->type}}</td>
      <td>{{$job->deadline}}</td>
      <td>
      {{-- <a class="btn btn-primary btn-sm" href="{{route ('jobs.details',[$job->id, $job->slug]) }}">View</a> --}}
      <a class="btn btn-primary btn-sm" href="{{ route('jobs.edit',[$job->id, $job->slug]) }}">Edit</a>
      <a class="btn btn-danger btn-sm" href="{{ route('jobs.destroy',$job->id) }}" onclick= "return confirm('Are you Sure you want to delete this job?');">Delete</a>
      </td>
    </tr>
    @endforeach
  </table>
</div>
</div>
</div>
</div>
@endsection