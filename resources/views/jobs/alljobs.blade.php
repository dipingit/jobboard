@extends('layouts.master')

@section('content')
    <div class="content-block">
			<!-- Browse Jobs -->
			<div class="section-full bg-white browse-job content-inner-2">
				<div class="container">
					@if(session()->has('message'))
						<div class="alert {{ session()->get('status') == 0 ? "alert-danger" : "alert-success" }}">
							<button type="button"
									class="close"
									data-dismiss="alert"
									aria-hidden="true">&times;
							</button>
							{!! session()->get('message') !!}
						</div>
					@endif
					<div class="row">
						<div class="col-xl-9 col-lg-8">
							<h5 class="widget-title font-weight-700 text-uppercase">Recent Jobs</h5>
                            @foreach($jobs as $job)
							<ul class="post-job-bx">
								<li>
									<a href="/jobs/{{ $job->id }}/{{ $job->slug }}">
										<div class="d-flex m-b30">
											<div class="job-post-company">
												<span><img src="{{asset('images/logo/icon1.png')}}"/></span>
											</div>
											<div class="job-post-info">
												<h4>{{ $job->position }}</h4>
												<ul>
													<li><i class="fa fa-map-marker"></i>{{ $job->address }}</li>
													<li><i class="fa fa-bookmark-o"></i>{{ $job->type }}</li>
													<li><i class="fa fa-clock-o"></i>Deadline: {{ $job->deadline }}</li>
												</ul>
											</div>
										</div>
										<div class="d-flex">
											<div class="job-time mr-auto">
												<span>{{ $job->type }}</span>
											</div>
											<div class="salary-bx">
												<span>$1200</span>
											</div>
										</div>
										<span class="post-like fa fa-heart-o"></span>
									</a>
								</li>
							</ul>
                            @endforeach
                            {!! $jobs->links() !!}
						</div>
							<div class="col-xl-3 col-lg-4">
								<div class="sticky-top">
									<form action="" method="GET">
										<div class="clearfix m-b30">
											<h5 class="widget-title font-weight-700 text-uppercase">Keywords</h5>
												<input type="text" name="title" class="form-control" placeholder="Search" value="{{ request()->title }}">
										</div>
										<div class="clearfix m-b30">
											<div class="clearfix">
												<div class="form-group">
													<select name="category_id" class="form-control">
														<option value="0">Select Category</option>
														@foreach(App\Models\Category::all() as $cat)
															<option value="{{$cat->id}}">{{$cat->name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="clearfix">
												<div class="form-group">
                                					<button class="site-button">Search</button>
                           						 </div>
											</div>
										</div>
									</form>
								</div>
							</div>
					</div>	
				</div>
			</div>
            <!-- Browse Jobs END -->
	</div>
@endsection



