@extends('layouts.master')
@section('content')
<br>
<div class="site-section bg-light pb-4 pt-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @foreach($applicants as $applicant)
                    <div class="card-header">{{$applicant->title}}</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>Applicant's Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Experience</th>
                                <th>Biodata</th>
                                <th>Resume</th>
                                <th>Cover Letter</th>
                            </thead>
                            <tbody>
                            @foreach($applicant->users as $user)
                                <tr>
                                    <td>{{$user->first_name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->profile->address}}</td>
                                    <td>{{$user->profile->experience}}</td>
                                    <td>{{$user->profile->bio}}</td>
                                    <td>
                                        <a href="{{Storage::url($user->profile->resume)}}">Resume</a>
                                    </td>
                                    <td>
                                        <a href="{{Storage::url($user->profile->cover_letter)}}">Cover Letter</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>    

@endsection