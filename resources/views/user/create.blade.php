@extends('layouts.master')
@section('content')
<div class="site-section bg-light pt-3 pb-3">
    <div class="container">
        @if(session()->has('message'))
            <div class="alert {{ session()->get('status') == 0 ? "alert-danger" : "alert-success" }}">
                <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true">&times;
                </button>
                {!! session()->get('message') !!}
            </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                @if(empty(Auth::user()->profile->avatar))
                    <img style="border-radius: 50px;width: 100%" src="{{asset('avatar/dummy.jpg')}}" width="100" height="200">
                @else
                    <img style="border-radius: 50px;width: 100%" src="{{asset('uploads/avatar')}}/{{Auth::user()->profile->avatar}}" width="100" height="200">
                @endif


                <div class="card">
                    <form enctype="multipart/form-data" action="{{route('profile.avatar')}}" method="post">
                        @csrf
                        @if(Session::has('avatar'))
                                <div class="alert alert-success">
                                    {{Session::get('avatar')}}
                                </div>
                            @endif
                        <div class="card-header">Upload Your Photo</div>
                        <div class="card-body">
                            <input type="file" class="form-control" name="avatar">
                            <br>
                            <button class="btn btn-primary">Update</button>
                        </div>
                    </form>
                    @if($errors->has('avatar'))
                        <div class="error" style="color: red">
                            {{$errors->first('avatar')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">Update Your Information</div>
                    <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-success">
                                    {{Session::get('message')}}
                                </div>
                            @endif
                        <form action="{{route('profile.store')}}" method="post">
                            @csrf
                        <div class="form-group">
                            <label for="">Address</label>
                            <textarea class="form-control" rows="3" name="address">{{Auth::user()->profile->address}}</textarea>
                        </div>
                            @if($errors->has('address'))
                                <div class="error" style="color: red">
                                    {{$errors->first('address')}}
                                </div>
                            @endif
                        <div class="form-group">
                        <label for="">Phone Number</label>
                            <input value="{{Auth::user()->profile->phone_number}}" type="text" name="phone_number" class="form-control">
                        </div>
                            @if($errors->has('phone_number'))
                                <div class="error" style="color: red">
                                    {{$errors->first('phone_number')}}
                                </div>
                            @endif
                        <div class="form-group">
                            <label for="">Experience</label>
                            <textarea class="form-control" rows="3" name="experience">{{Auth::user()->profile->experience}}
                            </textarea>
                        </div>
                            @if($errors->has('experience'))
                                <div class="error" style="color: red">
                                    {{$errors->first('experience')}}
                                </div>
                            @endif
                        <div class="form-group">
                            <label for="">BIODATA</label>
                            <textarea class="form-control" rows="3" name="bio">{{Auth::user()->profile->bio}}</textarea>
                        </div>
                            @if($errors->has('bio'))
                                <div class="error" style="color: red">
                                    {{$errors->first('bio')}}
                                </div>
                            @endif
                        <div class="form-group">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">User Details</div>
                    <div class="card-body">
                        <p><b>Name:</b> {{Auth::user()->first_name}} {{Auth::user()->last_name}}</p>
                        <p><b>Email:</b> {{Auth::user()->email}}</p>
                        <p><b>Address:</b> {{Auth::user()->profile->address}}</p>
                        <p><b>Phone Number:</b> {{Auth::user()->profile->phone_number}}</p>
                        <p><b>Experience:</b> {{Auth::user()->profile->experience}}</p>
                        <p><b>Biodata:</b> {{Auth::user()->profile->bio}}</p>
                        <p><b>Member Since:</b> {{date('F d Y', strtotime(Auth::user()->profile->created_at))}}</p>
                        @if(!empty(Auth::user()->profile->resume))
                            <p>
                                <a href="{{Storage::url(Auth::user()->profile->resume)}}" style="color:blue;">
                                    Download Resume
                                </a>
                            </p>
                        @else
                            <p style = "color:red"><b>Please Upload Your Resume</b></p>
                        @endif
                    </div>
                </div>
                <div class="card">
                    <form enctype="multipart/form-data" action="{{route('profile.resume')}}" method="post">
                        @csrf
                        @if(Session::has('resume'))
                          <div class="alert alert-success">
                              {{Session::get('resume')}}
                          </div>
                        @endif
                    <div class="card-header">Update Your Resume</div>
                    <div class="card-body">
                        <input type="file" class="form-control" name="resume">
                        <br>
                        <button class="btn btn-primary">Update</button>
                    </div>
                    </form>
                    @if($errors->has('resume'))
                        <div class="error" style="color: red">
                            {{$errors->first('resume')}}
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
@endsection