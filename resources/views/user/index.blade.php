@extends('layouts.app')
@section('content')
<div class="container">
<div class="card mx-auto">

  <div class="card-body">
    <img  class="mb-5"src="{{ asset('uploads/image') }}/{{ Auth::user()->profile->image }}"
    width="200" height="200">
    <h2>Name: {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
    <p>Skills: {{ $profile->skills }}</p>
  </div>
</div>
</div>
@endsection